package com.sriram.test.controllers;

import com.sriram.test.Exceptions.VendorNotFoundException;
import com.sriram.test.dtos.VendorDTO;
import com.sriram.test.services.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by navkumar on 12/02/17.
 *
 */
@RestController
@RequestMapping("api/vendor")
public final class VendorController {
    private  final VendorService vendorService ;



    @Autowired
    public VendorController(VendorService service) {
        this.vendorService = service;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    VendorDTO create(@RequestBody @Valid VendorDTO todoEntry) {
        return vendorService.create(todoEntry);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    VendorDTO delete(@PathVariable("id") String id) {
        return vendorService.delete(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    List<VendorDTO> findAll() {
        return vendorService.findAll();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    VendorDTO findById(@PathVariable("id") String id) {
        return vendorService.findById(id);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    VendorDTO update(@RequestBody @Valid VendorDTO todoEntry) {
        return vendorService.update(todoEntry);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleTodoNotFound(VendorNotFoundException ex) {
    }

}
