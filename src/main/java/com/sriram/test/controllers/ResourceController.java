package com.sriram.test.controllers;

import com.sriram.test.model.RepresentationData;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by navkumar on 12/02/17.
 */
@Controller
@RequestMapping("/health-check")
public class ResourceController {
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping(method= RequestMethod.GET)
    public @ResponseBody
    RepresentationData sayHello(@RequestParam(value="name", required=false, defaultValue="Stranger") String name) {
        return new RepresentationData(counter.incrementAndGet(), String.format(template, name));
    }
}
