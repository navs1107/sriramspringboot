package com.sriram.test.repository;

import com.sriram.test.model.VendorData;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by navkumar on 12/02/17.
 */
public interface VendorDataDao extends Repository<VendorData, String > {
    void delete(VendorData deleted) ;
    List<VendorData> findAll();
    Optional<VendorData> findOne(String id);
    VendorData save(VendorData saved);
}
