package com.sriram.test.dtos;

import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by navkumar on 12/02/17.
 * {@link com.sriram.test} objects.
 */
public final class VendorDTO {
    @Id
    private String id;

    @NotNull
    @Size(max = 10)
    private String name;

    @NotNull
    private String desc;

    public VendorDTO() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


}
