package com.sriram.test.services;

import com.sriram.test.dtos.VendorDTO;
import org.springframework.context.annotation.Bean;

import java.util.List;

/**
 * Created by navkumar on 12/02/17.
 * {@link com.sriram.test} objects.
 */
public interface VendorService {

    VendorDTO create(VendorDTO vendorDTO);

    VendorDTO delete(String id);

    List<VendorDTO> findAll();

    VendorDTO findById(String id);

    VendorDTO update(VendorDTO todo);
}
