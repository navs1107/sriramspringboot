package com.sriram.test.Exceptions;/**
 * ADOBE CONFIDENTIAL ___________________
 * <p>
 * Copyright 2011 Adobe Systems Incorporated All Rights Reserved.
 * <p>
 * NOTICE: All information contained herein is, and remains the property of
 * Adobe Systems Incorporated and its suppliers, if any. The intellectual and
 * technical concepts contained herein are proprietary to Adobe Systems
 * Incorporated and its suppliers and may be covered by U.S. and Foreign
 * Patents, patents in process, and are protected by trade secret or copyright
 * law. Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from Adobe
 * Systems Incorporated.
 *
 * @file ${FILE_NAME}
 * @author navkumar@adobe.com
 * @since 25/02/17
 */

/**
 * Created by navkumar on 25/02/17.
 */
public class VendorNotFoundException extends RuntimeException {

    public VendorNotFoundException(String id) {
        super(String.format("No Vendor entry found with id: <%s>", id));
    }

}
