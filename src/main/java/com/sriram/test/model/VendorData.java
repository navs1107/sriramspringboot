package com.sriram.test.model;

import org.springframework.data.annotation.Id;

import static org.springframework.util.Assert.notNull;

/**
 * Created by navkumar on 12/02/17.
 */
public final class VendorData {
    @Id
    private String id;
    private String name;
    private String desc;
    VendorData() {}
    private VendorData(VendorBuilder builder) {
        this.desc = builder.desc;
        this.name = builder.name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }
    public static VendorBuilder getBuilder() {
        return new VendorBuilder();
    }

    public static class VendorBuilder {
        private String name;
        private String desc;



        private VendorBuilder() {}
        public VendorBuilder name(String name) {
            this.name = name;
            return this;
        }
        public VendorBuilder desc(String desc) {
            this.desc = desc;
            return this;
        }
        public VendorData build(){
            VendorData vendorData = new VendorData(this);
            checkNameAndDesc(vendorData.getName(), vendorData.getDesc());
            return vendorData;
        }
        private void checkNameAndDesc(String name, String desc) {
            notNull(name, "Name can't be empty!");
        }
    }
}
