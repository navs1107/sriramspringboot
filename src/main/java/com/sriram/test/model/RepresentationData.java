package com.sriram.test.model;

import java.util.ArrayList;

/**
 * Created by navkumar on 12/02/17.
 */
public class RepresentationData {
    private final long id;
    private final String content;

    public RepresentationData(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}
