package com.sriram.test;

/**
 * Created by navkumar on 12/02/17.
 */
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

//@SpringBootApplication
@Configuration
@EnableAutoConfiguration
@ComponentScan
public class VendorDriverConfiguration {
    public static void main(String[] args) {
        SpringApplication.run(VendorDriverConfiguration.class, args);
    }

}
