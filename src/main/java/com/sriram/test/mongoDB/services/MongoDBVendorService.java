package com.sriram.test.mongoDB.services;

import com.sriram.test.Exceptions.VendorNotFoundException;
import com.sriram.test.dtos.VendorDTO;
import com.sriram.test.model.VendorData;
import com.sriram.test.repository.VendorDataDao;
import com.sriram.test.services.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

/**
 * Created by navkumar on 12/02/17.
 */
@Service
public final class MongoDBVendorService implements VendorService {
    private final VendorDataDao vendorDataDao;

    @Autowired
    public MongoDBVendorService(VendorDataDao vendorDataDao) {
        this.vendorDataDao = vendorDataDao;
    }

    @Override
    public VendorDTO create(VendorDTO vendorDTO) {
        VendorData persisted = VendorData.getBuilder().name(vendorDTO.getName()).desc(vendorDTO.getDesc()).build();
        persisted = vendorDataDao.save(persisted);

        return convertToDTO(persisted);
    }

    @Override
    public VendorDTO delete(String id) {
        return null;
    }

    @Override
    public List<VendorDTO> findAll() {
        //LOGGER.info("Finding all todo entries.");

        List<VendorData> todoEntries = vendorDataDao.findAll();

        //LOGGER.info("Found {} todo entries", todoEntries.size());

        return convertToDTOs(todoEntries);
    }

    @Override
    public VendorDTO findById(String id) {
        //LOGGER.info("Finding todo entry with id: {}", id);

        VendorData found = findTodoById(id);

        //LOGGER.info("Found todo entry: {}", found);

        return convertToDTO(found);
    }

    @Override
    public VendorDTO update(VendorDTO todo) {
        return null;
    }

    private VendorDTO convertToDTO(VendorData model) {
        VendorDTO dto = new VendorDTO();
        dto.setId(model.getId());
        dto.setName(model.getName());
        dto.setDesc(model.getDesc());
        return dto;
    }

    private List<VendorDTO> convertToDTOs(List<VendorData> models) {
        return models.stream()
                .map(this::convertToDTO)
                .collect(toList());
    }

    private VendorData findTodoById(String id) {
        Optional<VendorData> result = vendorDataDao.findOne(id);
        return result.orElseThrow(() -> new VendorNotFoundException(id));

    }
}
